

all:
	$(MAKE) gen-doc

gen-doc:
	rm -r docs/source
	cd docs && sphinx-apidoc -o source ../src
	cd docs && make html