import os
import re
import pandas as pd
import argparse

args = None


def parse_args():
    global args
    parser = argparse.ArgumentParser(description='Converter for converting logs to csv')
    parser.add_argument('-log-path', '--log-path', action='store', dest='log_path', type=str,
                        help='Path to the logfile')
    parser.add_argument('-output-file', '--output-file', action='store', dest='output_path', type=str,
                        default='logs.csv', help='Path to the csv file, that is used as output')

    args = parser.parse_args()


def parse_test_result(lines, idx):
    test_result = {}
    while idx < len(lines) and "-----------" not in lines[idx]:
        true_positives = re.search("True Positives: ((\-?\d+)*)", lines[idx])
        if true_positives:
            test_result["True Positives"] = true_positives.group(1)

        true_negatives = re.search("True Negatives: ((\-?\d+)*)", lines[idx])
        if true_negatives:
            test_result["True Negatives"] = true_negatives.group(1)

        false_positives = re.search("False Positives: ((\-?\d+)*)", lines[idx])
        if false_positives:
            test_result["False Positives"] = false_positives.group(1)

        false_negatives = re.search("False Negatives: ((\-?\d+)*)", lines[idx])
        if false_negatives:
            test_result["False Negatives"] = false_negatives.group(1)

        if "OneClassSVM" in lines[idx]:
            svm_parameters = ["kernel", "degree", "gamma", "nu", "coef0"]
            for param in svm_parameters:
                feature_param = re.search("{}=[\(]*(.+?),".format(param), lines[idx])
                if feature_param:
                    test_result["SVM {}".format(param)] = feature_param.group(1)

        if "HOG-FeatureExtractor" in lines[idx]:
            test_result["Feature Extractor"] = "hog"
            hog_parameters = ["orientations", "pixels_per_cell", "cells_per_block"]
            for param in hog_parameters:
                feature_param = re.search("{}=[\(]*(.+?),".format(param), lines[idx])
                if feature_param:
                    test_result["HOG {}".format(param)] = feature_param.group(1)

        if "LBP-FeatureExtractor" in lines[idx]:
            test_result["Feature Extractor"] = "lbp"
            hog_parameters = ["points", "radius"]
            for param in hog_parameters:
                feature_param = re.search("{}=[\(]*(.+?),".format(param), lines[idx])
                if feature_param:
                    test_result["LBP {}".format(param)] = feature_param.group(1)

        if "LocalMaxima-FeatureExtractor" in lines[idx]:
            test_result["Feature Extractor"] = "localMaxima"
            hog_parameters = ["min-distance", "size"]
            for param in hog_parameters:
                feature_param = re.search("{}=[\(]*(.+?),".format(param), lines[idx])
                if feature_param:
                    test_result["LM {}".format(param)] = feature_param.group(1)

        if "Plain-FeatureExtractor" in lines[idx]:
            test_result["Feature Extractor"] = "plain"
        idx = idx + 1

    return idx, test_result


def main():
    parse_args()

    if not os.path.exists(args.log_path):
        print("No such input file. Stopping processing")
        exit(0)

    file = open(args.log_path, 'r')
    lines = file.readlines()
    idx = 0

    test_results = []

    while idx < len(lines):
        line = lines[idx]
        if "-------------" in line:
            idx, test_result = parse_test_result(lines, idx + 1)
            if test_result:
                test_results.append(test_result)
        else:
            idx = idx + 1

    dataframe = pd.DataFrame(test_results)
    dataframe.to_csv(args.output_path, index=True, encoding='utf-8')

    file.close()


if __name__ == "__main__":
    main()
