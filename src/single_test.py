import itertools


from datasets.bosch_connector_loader import BoschConnectorPinsDataset
from feature_extractors.auto_encoder import AutoEncoderFeatureExtractor
from testing import multiprocess_training

dataset = BoschConnectorPinsDataset(patch_resize=(20, 20))
PARAMS_SVM_KERNEL = ["rbf", "linear", "poly", "sigmoid"]
PARAMS_SVM_DEGREE = [1, 2]
PARAMS_SVM_NU = [0.01, 0.1, 0.5]

# PARAMS_SVM_KERNEL = ["rbf"]
# PARAMS_SVM_DEGREE = [2]
# PARAMS_SVM_NU = [0.1]

FEATURE_EXTRACTORS = [AutoEncoderFeatureExtractor(
        model_store_path="../models/autoencoder_20x20px_singleEncoderDecoder128Features_v2.pt",
        dataset=dataset)]


def main():
    combinations = list(itertools.product(PARAMS_SVM_KERNEL, PARAMS_SVM_DEGREE, PARAMS_SVM_NU, FEATURE_EXTRACTORS))

    for svm_kernel, degree, nu, feature_extractor in combinations:
        multiprocess_training((svm_kernel, degree, 'scale', nu, 0.0, feature_extractor))


if __name__ == '__main__':
    main()
