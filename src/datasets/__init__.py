"""
Contains dataset classes to load various datasets.
"""

from numpy import ndarray


class DatasetLoader(object):
    """
    Abstract class for data loaders, that ensures, that some methods are implemented in the specific classes
    """

    def __init__(self):
        """
        Constructor of the abstract class
        """
        super(DatasetLoader, self).__init__()

    def train_data(self) -> ([ndarray], [ndarray]):
        """
        Returns the training image patches. It returns a list of positive_image samples as first return value and a
        list of negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                            value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        raise NotImplementedError

    def test_data(self) -> ([ndarray], [ndarray]):
        """
        Returns the test image patches. It returns a list of positive_image samples as first return value and a
        list of negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                    value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        raise NotImplementedError

    def train_test_split(self) -> ([ndarray], [ndarray], [ndarray], [ndarray]):
        """
        Returns a tuple with four values. Each value is a part of training, or test set (positive and negative). This
        method returns the single patches of the connector pins

        :return: Returns a tuple of lists (train_positives, train_negatives, test_positives, test_negatives)
        :rtype: ([numpy.ndarray], [numpy.ndarray], [numpy.ndarray], [numpy.ndarray])
        """
        train_pos, train_neg = self.train_data()
        test_pos, test_neg = self.test_data()
        return train_pos, train_neg, test_pos, test_neg
