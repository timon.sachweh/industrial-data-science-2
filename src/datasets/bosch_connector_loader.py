import os
import cv2

from datasets import DatasetLoader
from utils.image_utils import get_image_paths_for_dir, load_image_for_path, extract_image_patches_from_image, \
    boxes_for_connector_pins
from utils.db_utils import get_all_image_ids_by_set, get_image_by_id
from numpy import ndarray


class BoschConnectorPinsDataset(DatasetLoader):
    """
    Class for loading the BoschConnectorPins dataset

    :param data_dir: Base directory for the dataset, defaults to '../dataset'
    :type data_dir: String
    :param max_train: Value for specifing the maximum number of training_images. When None is set, all images will be
                    fetched, defaults to None
    :type max_train: int
    :param max_test: Value for specifing the maximum number of test_images. When None is set, all images will be
                    fetched, defaults to None
    :type max_test: int
    :param patch_resize: Resizes the patches to a specific size. If set to None explicitly, the size will not be
                            modified
    :type patch_resize: (int, int)
    """

    def __init__(self, data_dir="../dataset", max_train=None, max_test=None, patch_resize=(23, 23), use_db=False):
        """
        Constructor for setting up the BoschConnectorPinsDataset

        :param data_dir: Base directory for the dataset, defaults to '../dataset'
        :type data_dir: String, optional
        :param max_train: Value for specifing the maximum number of training_images. When None is set, all images will be
                    fetched, defaults to None
        :type max_train: int, optional
        :param max_test: Value for specifing the maximum number of test_images. When None is set, all images will be
                    fetched, defaults to None
        :param patch_resize: Resizes the patches to a specific size. If set to None explicitly, the size will not be
                            modified
        :type patch_resize: (int, int)
        :type max_test: int, optional
        :param use_db: Flag to enable use of database
        :type use_db: Boolean
        """
        super(BoschConnectorPinsDataset, self).__init__()
        self.data_dir = data_dir
        self.max_train = max_train
        self.max_test = max_test
        self.patch_resize = patch_resize
        self.use_db = use_db

    def train_data_paths(self) -> ([str], [str]):
        """
        Returns the image paths for training. The paths are devided in positive_image samples and negative_image
        samples. The first returned value contains the list with positive image examples and the second value the
        negative list.

        :return: Returns a list of paths to positive_image samples (first return value) and a list of paths to
                    negative_image samples
        :rtype: ([str], [str])
        """
        if self.use_db:
            pos_image_paths = get_all_image_ids_by_set("train_ok")
            neg_image_paths = get_all_image_ids_by_set("train_nok")
        else:
            pos_image_paths = get_image_paths_for_dir(os.path.join(self.data_dir, 'TRAIN/OK'))
            neg_image_paths = get_image_paths_for_dir(os.path.join(self.data_dir, 'TRAIN/NOK'))
        return pos_image_paths, neg_image_paths

    def test_data_paths(self) -> ([str], [str]):
        """
        Returns the image paths for test. The paths are devided in positive_image samples and negative_image
        samples. The first returned value contains the list with positive image examples and the second value the
        negative list.

        :return: Returns a list of paths to positive_image samples (first return value) and a list of paths to
                    negative_image samples
        :rtype: ([str], [str])
        """
        if self.use_db:
            pos_image_paths = get_all_image_ids_by_set("test_ok")
            neg_image_paths = get_all_image_ids_by_set("test_nok")
        else:
            pos_image_paths = get_image_paths_for_dir(os.path.join(self.data_dir, 'TEST/OK'))
            neg_image_paths = get_image_paths_for_dir(os.path.join(self.data_dir, 'TEST/NOK'))
        return pos_image_paths, neg_image_paths

    def train_images(self) -> ([ndarray], [ndarray]):
        """
        Returns the training images. It returns a list of positive_image samples as first return value and a list of
        negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                    value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        pos_image_paths, neg_image_paths = self.train_data_paths()
        if self.use_db:
            pos_images = [get_image_by_id(p) for p in pos_image_paths]
            neg_images = [get_image_by_id(p) for p in neg_image_paths]
        else:
            pos_images = [load_image_for_path(p) for p in pos_image_paths]
            neg_images = [load_image_for_path(p) for p in neg_image_paths]
        return pos_images, neg_images

    def test_images(self) -> ([ndarray], [ndarray]):
        """
        Returns the test images. It returns a list of positive_image samples as first return value and a list of
        negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                    value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        pos_image_paths, neg_image_paths = self.test_data_paths()
        if self.use_db:
            pos_images = [get_image_by_id(p) for p in pos_image_paths]
            neg_images = [get_image_by_id(p) for p in neg_image_paths]
        else:
            pos_images = [load_image_for_path(p) for p in pos_image_paths]
            neg_images = [load_image_for_path(p) for p in neg_image_paths]
        return pos_images, neg_images

    def train_patches(self) -> ([ndarray], [ndarray]):
        """
        Returns the training image patches. It returns a list of positive_image samples as first return value and a
        list of negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                    value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        pos_image_paths, neg_image_paths = self.train_data_paths()
        if self.use_db:
            pos_images = self.patches_for_image_sets(pos_image_paths)
            neg_images = self.patches_for_image_sets(neg_image_paths)
        else:
            pos_images = self.patches_for_image_paths(pos_image_paths)
            neg_images = self.patches_for_image_paths(neg_image_paths)
        return pos_images, neg_images

    def test_patches(self) -> ([ndarray], [ndarray]):
        """
        Returns the test image patches. It returns a list of positive_image samples as first return value and a
        list of negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                    value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        pos_image_paths, neg_image_paths = self.test_data_paths()
        if self.use_db:
            pos_images = self.patches_for_image_sets(pos_image_paths)
            neg_images = self.patches_for_image_sets(neg_image_paths)
        else:
            pos_images = self.patches_for_image_paths(pos_image_paths)
            neg_images = self.patches_for_image_paths(neg_image_paths)
        return pos_images, neg_images

    def data_for_path(self, path: str) -> (ndarray, [ndarray]):
        """
        Method for fetching the training image data from a given path. The method returns the full image as well as
        the single connector patches

        :param path: Absolute/relative path to the image on the computer
        :type path: String
        :return: Full image and list of single connector pins
        :rtype: (numpy.ndarray, [numpy.ndarray])
        """
        image = load_image_for_path(path)
        patches = extract_image_patches_from_image(image, boxes_for_connector_pins(image), size=self.patch_resize)
        normalized_patches = []
        for patch in patches:
            patch = cv2.normalize(patch, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
            normalized_patches.append(patch)
        return image, normalized_patches

    def patches_for_image_paths(self, paths) -> [ndarray]:
        """
        Method for getting all patches of a list of images. The method reads each image and fetches the patches of
        the connctor pins. The single lists of patches will be concatenated and returned as one long list

        :param paths: List of image paths (absolute, relative)
        :type paths: [String]
        :return: List of image patches
        :rtype: [numpy.ndarray]
        """
        overall_patches = []
        for path in paths:
            _, patches = self.data_for_path(path)
            overall_patches = overall_patches + patches
        return overall_patches

    def data_for_image(self, id: str) -> (ndarray, [ndarray]):
        """
        Method for fetching the training image data from a given path. The method returns the full image as well as
        the single connector patches

        :param id: image_id
        :type path: String
        :return: Full image and list of single connector pins
        :rtype: (numpy.ndarray, [numpy.ndarray])
        """
        image = get_image_by_id(id)
        patches = extract_image_patches_from_image(image, boxes_for_connector_pins(image), size=self.patch_resize)
        return image, patches

    def patches_for_image_sets(self, paths) -> [ndarray]:
        """
        Method for getting all patches of a list of images. The method reads each image and fetches the patches of
        the connctor pins. The single lists of patches will be concatenated and returned as one long list

        :param paths: List of image paths (absolute, relative)
        :type paths: [String]
        :return: List of image patches
        :rtype: [numpy.ndarray]
        """
        overall_patches = []
        for path in paths:
            _, patches = self.data_for_image(path)
            overall_patches = overall_patches + patches
        return overall_patches

    def train_data(self) -> ([ndarray], [ndarray]):
        """
        Returns the training image patches. It returns a list of positive_image samples as first return value and a
        list of negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                            value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        train_pos, train_neg = self.train_patches()
        return train_pos, train_neg

    def test_data(self) -> ([ndarray], [ndarray]):
        """
        Returns the test image patches. It returns a list of positive_image samples as first return value and a
        list of negative_images as second return value

        :return: Returns a list of positive images as first return value and a list of negative images as second return
                    value
        :rtype: ([numpy.ndarray], [numpy.ndarray])
        """
        test_pos, test_neg = self.test_patches()
        return test_pos, test_neg
