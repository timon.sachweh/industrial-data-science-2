import cv2

from datasets.bosch_connector_loader import BoschConnectorPinsDataset
from utils.image_utils import show_image
#from feature_extractors.plain import PlainFeatureExtractor as FeatureExtractor
from feature_extractors.lbp import LBPFeatureExtractor as FeatureExtractor
#from feature_extractors.canny import CannyFeatureExtractor as FeatureExtractor
#from feature_extractors.hog import HogFeatureExtractor as FeatureExtractor


def main():
    print("Testing the functionality of a feature extractor")

    dataset = BoschConnectorPinsDataset()

    train_pos, _ = dataset.train_data_paths()

    image, patches = dataset.data_for_path(train_pos[0])

    feature_extractor = FeatureExtractor()
    extracted_feature = feature_extractor.extract_feature(patches[0])

    print(extracted_feature)

    show_image(patches[0], "Test image")
    cv2.waitKey(0)


if __name__ == "__main__":
    main()
