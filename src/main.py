import cv2

from datasets.bosch_connector_loader import BoschConnectorPinsDataset
from sklearn.svm import OneClassSVM

from feature_extractors.plain import PlainFeatureExtractor
from testers.full_image import FullImageTester
from utils.models import ModelManagement
from utils.db_utils import register_new_run


def main():
    print("Starting training")
    config = input("Please give a short description of this run: ")
    runID = register_new_run(config)

    dataset = BoschConnectorPinsDataset()
    classifier = OneClassSVM()
    feature_extractor = PlainFeatureExtractor()

    model_management = ModelManagement(classifier=classifier, dataset=dataset, feature_extractor=feature_extractor,
                                       tester=[FullImageTester(dataset_loader=dataset)])
    model_management.train_model()
    model_management.test_model()


if __name__ == '__main__':
    main()
