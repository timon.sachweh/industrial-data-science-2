from numpy import ndarray
from skimage.feature import peak_local_max

from feature_extractors import FeatureExtractor


class LocalMaxFeatureExtractor(FeatureExtractor):
    """
    Specific feature extractor for local maxima type features

    :param min_distance: Number of points on radius
    :type min_distance: float
    :param indices: Boolean, if the coordinates of reflection point should be returned
    :type indices: bool
    :param size: Size of the patch, that should be extracted around the reflection point
    :type size: int
    """
    # Klasse sucht den Rfelektionspunkt des Pins und gibt einen vom Nutzer einstellbaren Bildausschnitt mit dem
    # Reflektionspunkt als Mittelpunkt zurück
    # min_distance = 4, damit nur der Reflektionspunkt selbst gefunden wird
    # indices = True damit Koordinaten des Reflektionspunktes zurück gegeben werden

    def __init__(self, min_distance: float = 4, indices: bool = True, size: int = 3):
        """
        Constructor for initializing the local maxima feature extractor

        :param min_distance: Number of points on radius
        :type min_distance: float
        :param indices: Boolean, if the coordinates of reflection point should be returned
        :type indices: bool
        :param size: Size of the patch, that should be extracted around the reflection point
        :type size: int
        """
        super(LocalMaxFeatureExtractor, self).__init__()
        self.min_distance = min_distance
        self.indices = indices
        self.size = size

    # findet alle lokalen peaks und gibt Koordinaten als ndarray zurück
    # Bilder müssen invertiert werden, da Maxima gesucht werden
    def find_peaks(self, img: ndarray) -> ndarray:
        inverted_img = 1-img
        peaks = peak_local_max(inverted_img, min_distance=self.min_distance, threshold_abs=None, threshold_rel=None,
                               exclude_border=6, indices=self.indices, footprint=None, labels=None)
        return peaks

    # gibt Bildausschnitt mit gesuchtem Peak als Zentrum in der Größe size x size zurück
    def extract_feature(self, img: ndarray):
        """
        Specific implementation to extract local maxima features for a given image

        :param img: Image, from which the features should be extracted
        :type img: numpy.ndarray
        :return: Feature vector
        :rtype: numpy.ndarray
        """
        columns, rows = self.build_extraction_matrix(img, self.size)[:]
        features = img[int(rows[0]):int(rows[1]), int(columns[0]):int(columns[1])]
        return features.flatten()

    # errechnet aus Koordinaten des Peaks die Koordinaten des Bildausschnitts
    def build_extraction_matrix(self, img: ndarray, size: int):
        x, y = self.select_wanted_peak(img)
        if size % 2:
            columns = [y - ((size - 1) / 2), y + ((size - 1) / 2 + 1)]
            rows = [x - (size - 1) / 2, x + (size - 1) / 2 + 1]
        else:
            columns = [y - size / 2, y + size / 2 + 1]
            rows = [x - size / 2, x + size / 2 + 1]
        return columns, rows

    # Falls mehrere Peaks gefunden werden, wird der zum Zentrum nächstgelegene gewählt
    def select_wanted_peak(self, img: ndarray):
        center = [12, 12]
        peaks = self.find_peaks(img)
        if len(peaks) > 0:
            wanted_peak = peaks[0]
            distance = lambda y, x: (y - center[0]) ** 2 + (x - center[1]) ** 2
            for peak in peaks:
                if distance(wanted_peak[0], wanted_peak[1]) > (distance(peak[0], peak[1])):
                    wanted_peak = peak
        else:
            wanted_peak = (img.shape[0] / 2, img.shape[1] / 2)
        return wanted_peak

    def params(self) -> str:
        """
        Method for describing all parameters of the feature extractor.
        For this extractor there are:
        - min-distance
        - size

        :return: Returns the describing string
        :rtype: String
        """
        return "LocalMaxima-FeatureExtractor: min-distance={}, size={}" \
            .format(self.min_distance, self.size)
