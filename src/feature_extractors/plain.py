from feature_extractors import FeatureExtractor
from numpy import ndarray
import numpy as np


class PlainFeatureExtractor(FeatureExtractor):
    """
    Specific feature extractor for flatten the image features
    """

    def extract_feature(self, img: ndarray) -> ndarray:
        """
        Specific implementation to flatten the image pixels for a given image

        :param img: Image, from which the features should be extracted
        :type img: numpy.ndarray
        :return: Feature vector
        :rtype: numpy.ndarray
        """
        if img is None:
            return np.empty(0)
        len_x, len_y = img.shape
        feature_vector = np.resize(img, new_shape=len_x * len_y)
        return feature_vector

    def params(self) -> str:
        """
        Method for describing all parameters of the feature extractor

        :return: Returns the describing string
        :rtype: String
        """
        return "Plain-FeatureExtractor: No specific features"
