from numpy import ndarray
from skimage.feature import canny

from feature_extractors import FeatureExtractor


class CannyFeatureExtractor(FeatureExtractor):
    """
    Specific feature extractor for canny edge features

    :param sigma: Parameter for controlling the sigma value of canny edge detector
    :type sigma: float
    :param low_threshold: Parameter for controlling the low_threshold
    :type low_threshold: float
    :param high_threshold: Parameter for controlling the high_threshold
    :type high_threshold: float
    """

    def __init__(self, sigma: float = 1, low_threshold: float = None, high_threshold: float = None):
        """
        Constructor for initializing the canny feature extractor

        :param sigma: Parameter for controlling the sigma value of canny edge detector
        :type sigma: float
        :param low_threshold: Parameter for controlling the low_threshold
        :type low_threshold: float
        :param high_threshold: Parameter for controlling the high_threshold
        :type high_threshold: float
        """
        super(CannyFeatureExtractor, self).__init__()
        self.sigma = sigma
        self.low_threshold = low_threshold
        self.high_threshold = high_threshold

    def extract_feature(self, img: ndarray) -> ndarray:
        """
        Specific implementation to extract hog features for a given image

        :param img: Image, from which the features should be extracted
        :type img: numpy.ndarray
        :return: Feature vector
        :rtype: numpy.ndarray
        """
        image = canny(image=img, sigma=self.sigma, low_threshold=self.low_threshold, high_threshold=self.high_threshold)
        # TODO: change return type from image to ndarray (1dimensional)
        return image
