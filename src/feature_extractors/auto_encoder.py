import os

from numpy import ndarray
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch
import numpy as np

from datasets import DatasetLoader
from datasets.bosch_connector_loader import BoschConnectorPinsDataset
from feature_extractors import FeatureExtractor


class AutoEncoderFeatureExtractor(FeatureExtractor, nn.Module):

    def __init__(self, model_store_path="../../models/autoencoder.pt",
                 dataset: DatasetLoader = BoschConnectorPinsDataset(),
                 criterion=nn.MSELoss(), num_epochs: int = 3000, batch_size: int = 8, learning_rate: float = 0.001):
        super().__init__()
        self.model_store_path = model_store_path
        self.train_data, _ = dataset.train_data()
        self.train_data = [img.flatten() for img in self.train_data]
        self.input_shape = len(self.train_data[0])
        self.criterion = criterion
        self.num_epochs = num_epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate

        self.enc1 = nn.Linear(in_features=self.input_shape, out_features=128)
        # self.enc2 = nn.Linear(in_features=256, out_features=128)
        # self.enc3 = nn.Linear(in_features=128, out_features=64)
        # self.enc4 = nn.Linear(in_features=64, out_features=32)
        # self.enc5 = nn.Linear(in_features=32, out_features=16)

        # self.dec1 = nn.Linear(in_features=16, out_features=32)
        # self.dec2 = nn.Linear(in_features=32, out_features=64)
        # self.dec3 = nn.Linear(in_features=64, out_features=128)
        # self.dec4 = nn.Linear(in_features=128, out_features=256)
        self.dec5 = nn.Linear(in_features=128, out_features=self.input_shape)

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.load_model()

    def extract_feature(self, img: ndarray) -> ndarray:
        img = img.flatten().astype(np.float32)
        img = torch.tensor(img)
        img = img.to(self.device)
        img = img.view(-1, len(img))

        img = self.encode(img)
        # generated = self.forward(img)
        # difference = (img - generated)**2
        # return difference.cpu().detach().numpy()[0]
        return img.cpu().detach().numpy()[0]

    def forward(self, x):
        x = self.encode(x)
        x = self.decode(x)
        return x

    def encode(self, x):
        x = F.relu(self.enc1(x))
        #  x = F.relu(self.enc2(x))
        # x = F.relu(self.enc3(x))
        # x = F.relu(self.enc4(x))
        # x = torch.relu(self.enc5(x))
        return x

    def decode(self, x):
        # x = F.relu(self.dec1(x))
        # x = F.relu(self.dec2(x))
        # x = F.relu(self.dec3(x))
        # x = F.relu(self.dec4(x))
        x = F.relu(self.dec5(x))
        return x

    def load_model(self):
        if os.path.exists(self.model_store_path):
            self.load_state_dict(torch.load(self.model_store_path))
            self.to(device=self.device)
        else:
            self.train_model()

    def save_model(self):
        torch.save(self.state_dict(), self.model_store_path)

    def train_model(self):
        self.to(self.device)
        optimizer = optim.Adam(self.parameters(), lr=self.learning_rate)

        train_loss = []
        for epoch in range(self.num_epochs):
            running_loss = 0.0
            for idx in range(0, len(self.train_data), self.batch_size):
                data = np.array(self.train_data[idx:idx+self.batch_size]).astype(np.float32)
                data = torch.tensor(data)
                data = data.to(self.device)
                optimizer.zero_grad()
                outputs = self.forward(data)
                loss = self.criterion(outputs, data)
                loss.backward()
                optimizer.step()
                running_loss += loss.item()
            loss = running_loss / len(self.train_data)
            train_loss.append(loss)

            print('Epoch {} of {}, Train Loss: {:.6f}'.format(
                epoch + 1, self.num_epochs, loss))

        self.save_model()

        return train_loss

    def params(self) -> str:
        """
        Method for describing all parameters of the feature extractor.
        For this extractor there are:
        - min-distance
        - size

        :return: Returns the describing string
        :rtype: String
        """
        return "AutoEncoder-FeatureExtractor: min-features={}, input-size={}" \
            .format(32, 400)


if __name__ == '__main__':
    feature_extractor = AutoEncoderFeatureExtractor(dataset=BoschConnectorPinsDataset(data_dir="../../dataset"))
