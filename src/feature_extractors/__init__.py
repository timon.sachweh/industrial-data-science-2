"""
Contains the different feature extractors, that will be used to extract the features from the image patches.
The resulting feature vector will be consumed by the classifier
"""

from numpy import ndarray


class FeatureExtractor(object):
    """
    Class/Interface, to standardize the method, for feature extraction
    """

    def __init__(self):
        """
        Basic constructor for each feature_extractor
        """
        super(FeatureExtractor, self).__init__()

    def extract_feature(self, img: ndarray) -> ndarray:
        """
        Template for method, that will be called during training process. The method should extract a feature vector
        from an image

        :param img: Is the image, from which, the feature vector should be extracted
        :type img: numpy.ndarray
        :return: feature_vector
        :rtype: numpy.ndarray
        """
        raise NotImplementedError

    def extract_features(self, images: [ndarray]) -> [ndarray]:
        """
        Calls method extract_feature for each feature

        :param images: Are the images, from which the feature should be extracted
        :type images: [numpy.ndarray]
        :return: List of the feature vectors
        :rtype: [numpy.ndarray]
        """
        return [self.extract_feature(img) for img in images]

    def params(self) -> str:
        """
        Method for describing all parameters of the feature extractor

        :return: Returns the describing string
        :rtype: String
        """
        return "Base Feature extractor"
