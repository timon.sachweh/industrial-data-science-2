from feature_extractors import FeatureExtractor
from numpy import ndarray
import numpy as np
from skimage.feature import local_binary_pattern


class LBPFeatureExtractor(FeatureExtractor):
    """
    Specific feature extractor for lbp type features

    :param points: Number of points on radius
    :type points: int
    :param radius: Size of radius of lbp descriptor
    :type radius: float
    """

    def __init__(self, points: int = 8, radius: float = 1):
        """
        Constructor for initializing the hog feature extractor

        :param points: Number of points on radius
        :type points: int
        :param radius: Size of radius of lbp descriptor
        :type radius: float
        """
        super().__init__()
        self.points = points
        self.radius = radius

    def extract_feature(self, img: ndarray) -> ndarray:
        """
        Specific implementation to extract lbp features for a given image

        :param img: Image, from which the features should be extracted
        :type img: numpy.ndarray
        :return: Feature vector
        :rtype: numpy.ndarray
        """
        lbp_features = local_binary_pattern(img, P=self.points, R=self.radius)

        (hist, _) = np.histogram(lbp_features.ravel(),
                                 bins=np.arange(0, self.points + 3),
                                 range=(0, self.points + 2))
        # normalize the histogram
        hist = hist.astype("float")
        hist /= (hist.sum() + 1e-10)
        return hist

    def params(self) -> str:
        """
        Method for describing all parameters of the feature extractor.
        For this extractor there are:
        - points
        - radius

        :return: Returns the describing string
        :rtype: String
        """
        return "LBP-FeatureExtractor: points={}, radius={}" \
            .format(self.points, self.radius)
