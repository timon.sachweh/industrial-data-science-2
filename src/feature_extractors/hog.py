from numpy import ndarray
from skimage.feature import hog

from feature_extractors import FeatureExtractor


class HogFeatureExtractor(FeatureExtractor):
    """
    Specific feature extractor for hog type features

    :param orientations: Number of bins of hog feature
    :type orientations: int
    :param pixels_per_cell: Number of pixels per cell in x and y direction
    :type pixels_per_cell: (int, int)
    :param cells_per_block: Number of cells per block in x and y direction
    :type cells_per_block: (int, int)
    """

    def __init__(self, orientations: int = 8, pixels_per_cell: (int, int) = (4, 4),
                 cells_per_block: (int, int) = (2, 2), visualize: bool = True):
        """
        Constructor for initializing the hog feature extractor

        :param orientations: Number of bins of hog feature
        :type orientations: int
        :param pixels_per_cell: Number of pixels per cell in x and y direction
        :type pixels_per_cell: (int, int)
        :param cells_per_block: Number of cells per block in x and y direction
        :type cells_per_block: (int, int)
        """
        super().__init__()
        self.orientations = orientations
        self.pixels_per_cell = pixels_per_cell
        self.cells_per_block = cells_per_block

    def extract_feature(self, img: ndarray) -> ndarray:
        """
        Specific implementation to extract hog features for a given image

        :param img: Image, from which the features should be extracted
        :type img: numpy.ndarray
        :return: Feature vector
        :rtype: numpy.ndarray
        """
        fd = hog(img, orientations=self.orientations, pixels_per_cell=self.pixels_per_cell,
                 cells_per_block=self.cells_per_block, visualize=False, multichannel=False,
                 feature_vector=True)
        return fd

    def params(self) -> str:
        """
        Method for describing all parameters of the feature extractor.
        For this extractor there are:
        - orientations
        - pixels_per_cell
        - cells_per_block

        :return: Returns the describing string
        :rtype: String
        """
        return "HOG-FeatureExtractor: orientations={}, pixels_per_cell={}, cells_per_block={}" \
            .format(self.orientations, self.pixels_per_cell, self.cells_per_block)
