import itertools
import multiprocessing
import os

import numpy as np

from datasets.bosch_connector_loader import BoschConnectorPinsDataset
from sklearn.svm import OneClassSVM

from feature_extractors.local_maxima import LocalMaxFeatureExtractor
from feature_extractors.plain import PlainFeatureExtractor
from feature_extractors.hog import HogFeatureExtractor
from feature_extractors.lbp import LBPFeatureExtractor
from testers.full_image import FullImageTester
from utils.models import ModelManagement
from utils.db_utils import register_new_run

PARAMS_SVM_KERNEL = ["rbf", "poly", "sigmoid", "linear"]  # ["rbf", "linear", "poly", "sigmoid"]
PARAMS_SVM_DEGREE = [1, 2, 3]
PARAMS_SVM_NU = [0.01, 0.1, 0.2, 0.35, 0.5]
PARAMS_SVM_GAMMA = ["auto", "scale", 0.1, 0.3]
PARAMS_SVM_COEF = [0.0, 0.1, 0.2, 0.4]

PARAMS_FEATURE_EXTRACTORS = [
    PlainFeatureExtractor(),
    # LBPFeatureExtractor(points=18, radius=2),
    HogFeatureExtractor(orientations=8, pixels_per_cell=(2, 2), cells_per_block=(2, 2)),
    HogFeatureExtractor(orientations=8, pixels_per_cell=(4, 4), cells_per_block=(2, 2)),
    LocalMaxFeatureExtractor(min_distance=3, size=5),
    LocalMaxFeatureExtractor(min_distance=3, size=7),
    LocalMaxFeatureExtractor(min_distance=3, size=9),
    LocalMaxFeatureExtractor(min_distance=3, size=10),
    LocalMaxFeatureExtractor(min_distance=3, size=12),
    LocalMaxFeatureExtractor(min_distance=3, size=15)
]

dataset = BoschConnectorPinsDataset(patch_resize=(20, 20))


def multiprocess_training(train_combinations):
    kernel, degree, gamma, nu, coef, feature_extractor = train_combinations
    classifier = OneClassSVM(kernel=kernel, degree=degree, gamma=gamma, nu=nu, coef0=coef)
    classifier_params = "OneClassSVM: kernel={}, degree={}, gamma={}, nu={}, coef0={}".format(kernel, degree, gamma,
                                                                                              nu, coef)

    model_management = ModelManagement(classifier=classifier, dataset=dataset, feature_extractor=feature_extractor,
                                       tester=[FullImageTester(dataset_loader=dataset)])
    try:
        model_management.train_model()
        model_management.test_model()
        # Printing parameters
        print("\n{}\n{}\n----------------------------------------------------------"
              .format(classifier_params, feature_extractor.params()))
    except Exception as ex:
        print("----------------------------------------------------------\nError while trying classifier:\n{}\n{}\n"
              "----------------------------------------------------------\n{}\n-------------------------------------"
              "---------------------"
              .format(classifier_params, feature_extractor.params(), ex))


def main():
    combinations = list(itertools.product(PARAMS_SVM_KERNEL, PARAMS_SVM_DEGREE, PARAMS_SVM_GAMMA, PARAMS_SVM_NU,
                                          PARAMS_SVM_COEF, PARAMS_FEATURE_EXTRACTORS))

    config = input("Please give a short description of this run: ")
    runID = register_new_run(config)

    count_subprocesses = min(multiprocessing.cpu_count(), 18)

    pool = multiprocessing.Pool(count_subprocesses)
    pool.map(multiprocess_training, combinations)


if __name__ == '__main__':
    main()
