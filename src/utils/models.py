from datasets import DatasetLoader
from feature_extractors import FeatureExtractor
from testers import ModelTester


class ModelManagement(object):
    """
    Class for managing the classification training and testing

    :param classifier: Classifier, that should be trained or tested
    :type classifier: sklearn.BaseEstimator
    :param dataset: Dataset loader, that is used for fetching the train data / test data
    :type dataset: DatasetLoader
    :param feature_extractor: Feature Extractor for extraction features from train data
    :type feature_extractor: FeatureExtractor
    :param tester: List of individual testers, that should be tested
    :type tester: [ModelTester]
    :param model_path: Path, where the model should be stored
    :type model_path: String
    :param enable_logging: Boolean for enabling detailed logging
    :type enable_logging: Boolean
    """

    def __init__(self, classifier, dataset: DatasetLoader, feature_extractor: FeatureExtractor,
                 tester: [ModelTester] = None, train_data_test: bool = False, test_data_test: bool = False,
                 model_path: str = "../models/default", enable_logging: bool = False):
        """
        Constructor for model management class

        :param classifier: Classifier, that should be trained or tested
        :type classifier: sklearn.BaseEstimator
        :param dataset: Dataset loader, that is used for fetching the train data / test data
        :type dataset: DatasetLoader
        :param feature_extractor: Feature Extractor for extraction features from train data
        :type feature_extractor: FeatureExtractor
        :param tester: List of individual testers, that should be tested
        :type tester: [ModelTester]
        :param train_data_test: Boolean, if Test should be done on train data
        :type train_data_test: Boolean
        :param test_data_test: Boolean, if Test should be done on test data
        :type test_data_test: Boolean
        :param model_path: Path, where the model should be stored
        :type model_path: String
        :param enable_logging: Boolean for enabeling detailed logging
        :type enable_logging: Boolean
        """
        self.classifier = classifier
        self.dataset = dataset
        self.feature_extractor = feature_extractor
        self.tester = tester
        self.model_path = model_path
        self.enable_logging = enable_logging
        self.train_data_test = train_data_test
        self.test_data_test = test_data_test

    def train_model(self):
        """
        Trains the classifier with the train data from the DatasetLoader and FeatureExtractor
        """
        if self.enable_logging:
            print("Starting model training")
        train_pos, _ = self.dataset.train_data()
        if self.enable_logging:
            print("Starting feature extraction")
        train_pos = self.feature_extractor.extract_features(train_pos)
        if self.enable_logging:
            print("Finished feature extraction")
        self.classifier.fit(train_pos)
        if self.enable_logging:
            print("Finished model training")

    def test_model(self):
        """
        Tests the trained classifier with the custom testers. If no custom testers are set, the test data from the
        DatasetLoader will be used for evaluating true_positives, true_negatives, false_positives, false_negatives
        """
        do_probabilities = hasattr(self.classifier, "predict_proba")
        probabilities = None
        if self.tester is not None:
            for test in self.tester:
                for data in test:
                    feature = self.feature_extractor.extract_features(data)
                    predicted_class = self.classifier.predict(feature)
                    if do_probabilities:
                        probabilities = self.classifier.predict_proba(feature)
                    test.store_part_test_prediction(predicted_class, probabilities)
                test.store_test_results()
        # running basic test on train data from dataset
        if self.train_data_test:
            train_data_pos, _ = self.dataset.train_data()
            train_data_pos = self.feature_extractor.extract_features(train_data_pos)
            pos_pred = self.classifier.predict(train_data_pos)
            sum_true_pos = sum(pos_pred == 1.0)
            sum_false_neg = sum(pos_pred != 1.0)
            print("Train results: \n\t True Positives: {}\n\t False Negatives: {}"
                  .format(sum_true_pos, sum_false_neg))

        # running basic test on test data from dataset
        if self.test_data_test:
            test_data_pos, test_data_neg = self.dataset.test_data()
            test_data_pos = self.feature_extractor.extract_features(test_data_pos)
            test_data_neg = self.feature_extractor.extract_features(test_data_neg)
            pos_pred = self.classifier.predict(test_data_pos)
            neg_pred = self.classifier.predict(test_data_neg)

            sum_true_pos = sum(pos_pred == 1.0)
            sum_true_neg = sum(neg_pred == -1.0)
            sum_false_neg = sum(pos_pred != 1.0)
            sum_false_pos = sum(neg_pred != -1.0)
            print("Test results: \n\t True Positives: {}\n\t True Negatives: {} \n\t False Positives: {}\n\t "
                  "False Negatives: {}".format(sum_true_pos, sum_true_neg, sum_false_pos, sum_false_neg))

    def _store_model(self):
        raise NotImplementedError
