import psycopg2
import glob
import os, os.path
import sys
from numpy import ndarray
import cv2
from PIL import Image
                    

def writeImage(data):
    fout = None
    try:
        fout = open('record.png', 'wb')
        fout.write(data)
    except IOError as e:
        print(f"Error {0}")
        sys.exit(1)
    finally:
        if fout:
            fout.close()

def get_all_image_ids_by_set(set:str) -> [str]:
    """
    Returns all images of a given set:

    :param set: choose between training and test
    :type set: String
    :return: List of strings, that contain the image ids
    :rtype: [String]
    """

    try:
        connection = psycopg2.connect(user="indas2",
                        password="crispdm",
                        host="localhost",
                        port="5432",
                        database="connector_pins")  
        cursor = connection.cursor()
        if set == "train_ok":
            postgreSQL_select_Query = "select connector_id from connector_pins_main where is_train and is_ok"
        elif set == "train_nok":
            postgreSQL_select_Query = "select connector_id from connector_pins_main where is_train and not is_ok"
        elif set == "test_ok":
            postgreSQL_select_Query = "select connector_id from connector_pins_main where is_test"
        elif set == "test_nok":
            postgreSQL_select_Query = "select connector_id from connector_pins_main where is_test and not is_ok"
        else:
            sys.exit('Invalid set has been given')

        cursor.execute(postgreSQL_select_Query)
        print("Selecting rows from table using cursor.fetchall")
        mobile_records = cursor.fetchall() 

        connection.commit()
        count = cursor.rowcount
        print (count, "Records extracted from table")
    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to extract record from table", error)

    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
    
    ids = []
    for item in mobile_records:
        ids.append(item[0])

    return ids

    

def get_image_by_id(id:str) -> ndarray:
    """
    Returns an image of a given id:

    :param id: image id
    :type id: String
    :return: Loaded image as ndarray
    :rtype: numpy.ndarray
    """
    try:
        connection = psycopg2.connect(user="indas2",
                        password="crispdm",
                        host="localhost",
                        port="5432",
                        database="connector_pins")  
        cursor = connection.cursor()
        postgreSQL_select_Query = """select connector_pic from connector_pins_main where connector_id = %s LIMIT 1"""
        cursor.execute(postgreSQL_select_Query, (int(id),))
        print("Selecting rows from table using cursor.fetchall")
        mobile_records = cursor.fetchone()[0]

        connection.commit()
        count = cursor.rowcount
        print (count, "Records extracted from table")
    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to extract record from table", error)

    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")

    #Finde diese Lösung uncool. Habs aber nicht hinbekommen das Objekt direkt aus dem Speicher zu lesen.
    writeImage(mobile_records)
    image = cv2.imread('record.png', cv2.IMREAD_GRAYSCALE)
    os.remove("record.png")
    return 255 - image


def register_new_run(config:str) -> int:
    """
    Registers a new run in database, returns run ID:

    :param config: String describing the config
    :type config: String
    :return: Run ID
    :rtype: int
    """
    try:
        connection = psycopg2.connect(user="indas2",
                                password="crispdm",
                                host="localhost",
                                port="5432",
                                database="connector_pins")    
        cursor = connection.cursor()
        postgres_select_query = """SELECT COUNT(*) FROM run_configs"""
        cursor.execute(postgres_select_query)

        connection.commit()
        result = cursor.fetchone()
        runid = result[0]
        print (runid, "Rows counted")
        if runid < 0:
            sys.exit('Error')

        postgres_insert_query = """ INSERT INTO run_configs (run_id, run_config) VALUES (%s,%s)"""
        record_to_insert = (runid, config)
        cursor.execute(postgres_insert_query, record_to_insert)

        connection.commit()
        count = cursor.rowcount
        print (count, "Record inserted successfully into config table")

    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to insert record into table", error)

    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
    return runid

def upload_result_for_connector_pin(conn_id:str, run_id:int, is_ok:bool, time:int):
    """
    Uploads result for certain connector/run combination

    :param conn_id: Connector ID
    :type config: String
    :param run_id: ID of test run
    :type config: Integer
    :param is_ok: Result if the connector is ok
    :type config: Boolean
    :param time: Time that has passed until result
    :type config: Integer
    """
    try:
        connection = psycopg2.connect(user="indas2",
                                password="crispdm",
                                host="localhost",
                                port="5432",
                                database="connector_pins")    
        cursor = connection.cursor()
        postgres_insert_query = """ INSERT INTO test_run (connector_id, run_id, is_ok, decision_time) VALUES (%s,%s,%s,%s)"""
        record_to_insert = (int(conn_id), run_id, is_ok, time)
        cursor.execute(postgres_insert_query, record_to_insert)

        connection.commit()
        count = cursor.rowcount
        print (count, "Record inserted successfully into test run table")

    except (Exception, psycopg2.Error) as error :
        if(connection):
            print("Failed to insert record into table", error)

    finally:
        #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")