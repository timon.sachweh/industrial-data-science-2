import os

import cv2
import glob

from numpy import ndarray


def get_image_paths_for_dir(path: str) -> [str]:
    """
    Returns all images of a given path. If the path does not exist, an empty list will be returned

    :param path: Is the absolute/relative path to the directory with the images
    :type path: String
    :return: List of strings, that contain the paths to the individual images
    :rtype: [String]
    """
    if os.path.exists(path):
        return glob.glob('{}/*.png'.format(path))
    else:
        return []


def load_image_for_path(path: str) -> ndarray:
    """
    Loads the image from a given path, if it exist. The image will be converted in greyscale and inverted.

    :param path: Path to the image (relative/absolute)
    :type path: String
    :return: Loaded image as ndarray
    :rtype: numpy.ndarray
    :exception FileNotFoundError: Thrown, if the image at the given path does not exist
    """
    if os.path.exists(path):
        image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        return 255 - image
    else:
        raise FileNotFoundError("No image at path: {}".format(path))


def show_image_from_path(path: str):
    """
    Loads an image from given path (by calling load_image_for_path() function) and show the image with the show_image
    function

    :param path: Is the path to the image, that should be shown
    :type path: String
    """
    img = load_image_for_path(path)
    show_image(img=img, title=path)


def show_image(img: ndarray, title: str = "No title"):
    """
    Shows a given image (provided as ndarray). It is possible to specify a title for the image window

    :param img: Is the given image as ndarray
    :type img: numpy.ndarray
    :param title: Is the optional Title of the shown window. When not specified, the default title will be 'No Title'
    :type title: String, optional
    """
    cv2.imshow(title, img)


def boxes_for_connector_pins(img: ndarray, margin: int = 2) -> [(int, int, int, int)]:
    """
    Extracts the bounding boxes of the single connector points. First the contours will be extracted and for each
    contour, the outer border will be extracted in rectangular shape. The extracted boxes could be extended, to add a
    margin around the connector point

    :param img: Is the reference image, on which, the connector points should be extracted
    :type img: numpy.ndarray
    :param margin: Is an optional margin, that could be specified for the bounding boxes. Default is a margin of 2
    :type margin: int, optional
    :return: Returns a list of type [(int, int, int, int)]. The tuple consists of 2 points (x_1, y_1, x_2, y_2)
    :rtype: [(int, int, int, int)]
    """
    contours, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    connector_pin_areas = []
    for contour in contours:
        contour = contour[:, 0, :]
        x_1, y_1 = contour.min(axis=0)
        x_2, y_2 = contour.max(axis=0)

        x_1 = max(0, x_1 - margin)
        y_1 = max(0, y_1 - margin)
        x_2 = max(0, x_2 + margin)
        y_2 = max(0, y_2 + margin)
        connector_pin_areas.append((x_1, y_1, x_2, y_2))
    return connector_pin_areas


def extract_image_patches_from_image(img: ndarray, patches: [(int, int, int, int)],
                                     size: (int, int) = None) -> [ndarray]:
    """
    Extracts image patches from a given image (img). The patches are specified via the list of tuples
    (int, int, int, int), which contains (x_1, y_1, x_2, y_2) point information of the bounding boxes.
    The extracted patches are returned as list of ndarrays.

    :param img: Is the reference image, from which, the patches will be extracted
    :type img: numpy.ndarray
    :param patches: Are the point information for each connector point to extract the bounding box
    :type patches: [(int, int, int, int)]
    :param size: Are size dimension, to which the patches should be resized. If set to None, the patch will not be
                resized
    :type size: [(int, int)]
    :return: Returns the patches as list of ndarrays
    :rtype: [numpy.ndarray]
    """
    image_patches = []
    for patch in patches:
        assert len(patch) == 4
        patch = img[patch[1]:patch[3], patch[0]:patch[2]]
        if size is not None:
            patch = cv2.resize(patch, dsize=size, interpolation=cv2.INTER_CUBIC)
        image_patches.append(patch)
    return image_patches


def patches_for_image(img: ndarray, margin: int = 2, size: (int, int) = (23, 23)):
    """
    Returns the patches of a given image. It leaves a margin with the size of the given parameter and resizes each patch
    to the given size.

    :param img: Image, from which the patches should be extracted
    :type img: numpy.ndarray
    :param margin: Margin, that should be left around the pin
    :type margin: int
    :param size: Size to which the patches should be resized
    :type size: (int, int)
    :return:
    """
    return extract_image_patches_from_image(img, boxes_for_connector_pins(img, margin=margin), size=size)
