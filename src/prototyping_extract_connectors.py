import cv2
from skimage import filters

from datasets.bosch_connector_loader import BoschConnectorPinsDataset
from utils.image_utils import show_image, load_image_for_path, extract_image_patches_from_image, \
    boxes_for_connector_pins


def main():
    dataset = BoschConnectorPinsDataset()

    train_pos, _ = dataset.train_data_paths()

    image = load_image_for_path(train_pos[0])

    show_image(image, "Base image")

    inverted_image = 255 - image
    show_image(inverted_image, "Inverted image")

    sobel = filters.sobel(image)
    show_image(sobel, "Sobel borders")

    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv_contours_img = image.copy()
    cv2.drawContours(cv_contours_img, contours, -1, (255, 255, 255), 1)

    margin = 2

    # create rect from contour
    patch_boxes = boxes_for_connector_pins(image, margin=margin)
    for (x_1, y_1, x_2, y_2) in patch_boxes:
        cv2.rectangle(cv_contours_img, (x_1, y_1), (x_2, y_2), (255, 255, 255), 1)
    show_image(cv_contours_img, "Cv-contours image")

    for idx, patch in enumerate(extract_image_patches_from_image(image, patch_boxes)):
        show_image(patch, "Patch {}".format(idx))

    cv2.waitKey(0)


if __name__ == '__main__':
    main()
