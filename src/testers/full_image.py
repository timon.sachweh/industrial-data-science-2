from datasets.bosch_connector_loader import BoschConnectorPinsDataset
from testers import ModelTester
from utils.image_utils import patches_for_image


class FullImageTester(ModelTester):
    """
    Class that inherits from ModelTester. Is a specific tester for testing a classificator against the full
    bosch connector pin images

    :param dataset_loader: Dataset loader for fetching the image data
    :type dataset_loader: BoschConnectorPinsDataset
    :param full_logging: Controls, if the processing should output detailed logging information
    :type full_logging: bool
    """

    def __init__(self, dataset_loader: BoschConnectorPinsDataset, full_logging=False):
        """
        Constructor for a tester, that handles the full images from the dataset

        :param dataset_loader: Dataset loader for fetching the image data
        :type dataset_loader: BoschConnectorPinsDataset
        :param full_logging: Controls, if the processing should output detailed logging information
        :type full_logging: bool
        """
        super().__init__()
        self.dataset_loader = dataset_loader
        self.full_logging = full_logging

        self.pos_images, self.neg_images = self.dataset_loader.test_images()
        self.current_index = 0
        self.last_prediction_class = 1

        self.true_positives = 0
        self.true_negatives = 0
        self.false_positives = 0
        self.false_negatives = 0

    def next_test_data(self):
        """
        Method, that returns the patches for the current image. The current image index will be increased after every
        method call. The corresponding class, that the classifier should predict will be stored locally, so that the
        predictions could be evaluated in store_part_test_prediction.

        :return: next test data image patches (for one image)
        """
        if self.current_index < len(self.pos_images):
            # return pos image example
            test_data = patches_for_image(self.pos_images[self.current_index], size=self.dataset_loader.patch_resize)
            self.last_prediction_class = 1
        elif self.current_index - len(self.pos_images) < len(self.neg_images):
            # return neg image example
            test_data = patches_for_image(self.neg_images[self.current_index - len(self.pos_images)],
                                          size=self.dataset_loader.patch_resize)
            self.last_prediction_class = -1
        else:
            raise StopIteration
        self.current_index += 1
        return test_data

    def store_part_test_prediction(self, classification, confidence):
        """
        Method is called after every single prediction of test data image patches. Evaluates the results of this
        individual prediction.

        :param classification: Classification results of the classifier prediction
        :type classification: numpy.ndarray
        :param confidence: Confidence values for the classes, if this functionality is implemented in the classifier
        :type confidence: numpy.ndarray
        """
        minimum_class = min(classification)
        if minimum_class == 1 and self.last_prediction_class == 1:
            self.true_positives += 1
        elif minimum_class == -1 and self.last_prediction_class == -1:
            self.true_negatives += 1
        elif minimum_class == 1 and self.last_prediction_class == -1:
            self.false_positives += 1
        elif minimum_class == -1 and self.last_prediction_class == 1:
            self.false_negatives += 1
        if self.full_logging:
            print("Prediction: {} (test image: {}/{}) \t {}".format(minimum_class, self.current_index,
                                                                    len(self.pos_images) + len(self.neg_images),
                                                                    classification))

    def store_test_results(self):
        """
        Method that is called after full test iteration. The results will be printed to the terminal
        """
        print("Test results: \n\t True Positives: {}\n\t True Negatives: {} \n\t False Positives: {}\n\t "
              "False Negatives: {}".format(self.true_positives, self.true_negatives, self.false_positives,
                                           self.false_negatives))
