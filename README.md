# Projektumgebung für das Industrial Data Science 2 Projekt: Bosch Connector Pins

## Dokumentation

Die Dokumentation wird mittels Sphinx aus den Python-Doc Kommentaren generiert. 
Sie ist über [Gitlab-Pages](https://timon.sachweh.gitlab.io/industrial-data-science-2/) 
einsehbar.

## Projektsetup

1. Das Projekt muss geklont werden: 
    - SSH: `git@gitlab.com:timon.sachweh/industrial-data-science-2.git`
    - HTTPS: `https://gitlab.com/timon.sachweh/industrial-data-science-2.git`
2. Die von Bosch bereitgestellten Daten müssen in den __industrial-data-science-2__ Ordner im Root-Verzeichnis 
    als __dataset__ Ordner eingefügt werden.

## Datenbank

1. Docker + Docker Compose installieren (https://docs.docker.com/engine/install/, https://docs.docker.com/compose/install/)
2. In der Kommandozeile im Projektordner "docker-compose -f ./database/docker-compose.yml up -d" eingeben

Die Datenbank lauscht auf dem Port 5432, das Admin-Interface auf 8443
    
## Verzeichnisstruktur

- dataset
    - TEST
        - NOK
            - B-Test_NOK
        - OK
    - TRAIN
        - OK
- notebooks