.. Industrial Data Science 2: Bosch Connector Pins documentation master file, created by
   sphinx-quickstart on Fri May 15 00:27:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Industrial Data Science 2: Bosch Connector Pins's documentation!
===========================================================================

.. toctree::
   :maxdepth: 5
   :caption: Contents:

   source/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
