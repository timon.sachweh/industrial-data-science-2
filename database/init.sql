CREATE TABLE IF NOT EXISTS connector_pins_main (
    connector_id int NOT NULL,
    connector_pic bytea NOT NULL,
    is_train boolean,
    is_test boolean,
    is_ok boolean,
    metric varchar(40),
    time timestamp,
    PRIMARY KEY(connector_id)
);

CREATE TABLE IF NOT EXISTS run_configs (
    run_id int NOT NULL,
    run_config varchar(100) NOT NULL, 
    PRIMARY KEY(run_id)
);

CREATE TABLE IF NOT EXISTS test_run (
    connector_id int NOT NULL,
    run_id int,
    is_ok boolean,
    decision_time int,
    PRIMARY KEY(connector_id, run_id)
);