import psycopg2
import glob
import os, os.path
import sys
from PIL import Image
import time


def readImage(path: str):
    try:
        fin = open(path, "rb")
        img = fin.read()
        return img
    except IOError:
        print("Error")
        sys.exit(1)
    finally:
        if fin:
            fin.close()

def convertTimestamp(stamp: str) -> [str]:
    conv = stamp.split("-")
    date = conv[0].replace("_","-")
    time = conv[1].split(".")[0]
    time = time.replace("_",":")
    return date + " " + time

time.sleep(10)

dataset = ["/TRAIN/OK","/TEST/OK","/TEST/NOK"]
for path in dataset:
    path = "/dataset/" + path
    images = glob.glob('{}/*.png'.format(path))
    for i in images:
        infos = i.split(",")
        i_id = infos[3].strip(".png").split("=")[1]
        if("mod" in i_id):
            print (i_id, "Record is modified, skipping")
            continue
        i_id = int(i_id)
        i_pic = readImage(i)
        i_is_train = True if "TRAIN" in infos[0] else False
        i_is_test = True if "TEST" in infos[0] else False
        i_is_ok = False if "NOK" in infos[0] else True
        time_unformat = infos[0].split("=")[1]
        i_time = convertTimestamp(time_unformat)
        try:
            connection = psycopg2.connect(user="indas2",
                                            password="crispdm",
                                            host="postgres",
                                            port="5432",
                                            database="connector_pins")
            cursor = connection.cursor()
            i_metric = "Connector Pin"
            postgres_insert_query = """ INSERT INTO connector_pins_main (connector_id, connector_pic, is_train, is_test, is_ok, metric, time) VALUES (%s,%s,%s,%s,%s,%s,%s)"""
            binary_pic = psycopg2.Binary(i_pic)
            record_to_insert = (i_id, binary_pic, i_is_train, i_is_test, i_is_ok, i_metric, i_time)
            cursor.execute(postgres_insert_query, record_to_insert)

            connection.commit()
            count = cursor.rowcount
            print (count, "Record inserted successfully into mobile table")

        except (Exception, psycopg2.Error) as error :
            if(connection):
                print("Failed to insert record into mobile table", error)

        finally:
            #closing database connection.
            if(connection):
                cursor.close()
                connection.close()
                print("PostgreSQL connection is closed")
