FROM jupyter/minimal-notebook

RUN pip install --upgrade pip && \
    pip install numpy matplotlib opencv-python scikit-image Sphinx sphinx-rtd-theme sklearn scikit-learn